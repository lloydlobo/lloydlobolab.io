# lloydlobo.gitlab.io

[![pipeline status](https://gitlab.com/lloydlobo/lloydlobo.gitlab.io/badges/main/pipeline.svg)](https://gitlab.com/lloydlobo/lloydlobo.gitlab.io/-/commits/main) · [![coverage report](https://gitlab.com/lloydlobo/lloydlobo.gitlab.io/badges/main/coverage.svg)](https://gitlab.com/lloydlobo/lloydlobo.gitlab.io/-/commits/main) · [![Latest Release](https://gitlab.com/lloydlobo/lloydlobo.gitlab.io/-/badges/release.svg)](https://gitlab.com/lloydlobo/lloydlobo.gitlab.io/-/releases)

This is the source code for my personal digital home.

## CI/CD

- The `build` pipeline runs a naive hand-coded Python
  minifier, `scripts/main.py`, on `html` and `css` files found in the `content/`
  directory.
    - `scripts/minifier.py` simply removes all comments and compresses the code
      by removing redundant white-space that makes for readability during
      development.
    - Additionally, it copies assets into the `public/` artifacts directory
      during the `build` job.
- The `deploy` pipeline uses GitLab's shared hosting and serves the artifacts
  from the previous `build` job in the `public/` directory.

**NOTE**: This process is not necessary for the website to work. Serving the
`content/` directory will work too, the difference being the absence of
somewhat minified files.

<!-- ## FYI

The table above primarily focuses on the traditional Command Prompt commands.

Here's a table comparing common Linux and Windows command-line commands:

| Linux Command | Windows Command |
|---------------|-----------------|
| `ls`          | `dir`           |
| `cat`         | `type`          |
| `mkdir`       | `mkdir`         |
| `rm`          | `del`           |
| `cp`          | `copy`          |
| `mv`          | `move`          |
| `top`         | `tasklist`      |
| `ps`          | `tasklist` or `ps` |
| `kill`        | `taskkill`      |
| `chmod`       | `icacls`        |

Please note that while many commands are similar, there are differences in
syntax and functionality between Linux and Windows commands. 

Additionally, Windows PowerShell provides a more powerful and flexible 
command-line interface compared to the traditional Command Prompt, 
and it has its own set of commands and syntax.  -->
