# -*- coding: utf-8 -*-

from minify import run_minify

if __name__ == "__main__":
    exit(run_minify())
