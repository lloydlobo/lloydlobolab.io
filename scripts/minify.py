# -*- coding: utf-8 -*-

from logging import Formatter, StreamHandler, getLogger, ERROR
from os import path, environ, makedirs
from re import sub, DOTALL
from shutil import copytree

from common import ProjectPaths

# ------------------------------------------------------------------------------
# CONSTANTS.

FLAG_CAN_OVERRIDE_DEV_ENV: bool = False
ENV: str = environ.get("ENVIRONMENT", "development")
PRODUCTION_ENV_: bool = (
    True if FLAG_CAN_OVERRIDE_DEV_ENV else (ENV == 'production'))

PATHS = ProjectPaths(path.dirname(path.dirname(__file__)))

# ------------------------------------------------------------------------------
# SETUP LOGGING HANDLER.

logger = getLogger(__name__)
logger.setLevel(ERROR)
handler = StreamHandler()
logger.addHandler(handler)
logger_formatter = Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(logger_formatter)


def minify_css(data: str) -> str:
    data = sub(r'/\*[\*[\s\S]*?\*/', '', data)
    data = sub(r'\s+', ' ', data)
    data = sub(r'\s*([:;{}])\s*', r'\1', data)
    data = sub(r'(\s+!important)', r'\1', data)
    return data.strip()


def minify_html(data: str) -> str:
    data = sub(r'<!--(.*?)-->', '', data, flags=DOTALL)
    data = sub(r'\s+', ' ', data)
    data = sub(r'>\s*<', '><', data)
    return data.strip()


def run_minify() -> int:
    # --------------------------------------------------------------------------
    # VALIDATE FILES IN CONTENT DIR THEN CREATE PUBLIC BUILD DIR.

    if not path.exists(PATHS.content_style_css):
        logger.error(f"{PATHS.content_style_css} does not exist")
        return 1

    if not path.exists(PATHS.content_index_html):
        logger.error(f"{PATHS.content_index_html} does not exist")
        return 1

    # makedirs(PATHS.public_dir, exist_ok=True)

    # --------------------------------------------------------------------------
    # MINIFY CSS AND HTML FILES.

    with open(PATHS.content_style_css, 'r', encoding='utf-8') as css_src_file:
        css_content = css_src_file.read()
        with open(PATHS.public_style_css, 'w',
                  encoding='utf-8') as css_dst_file:
            css_dst_file.write(
                minify_css(css_content) if PRODUCTION_ENV_ else css_content)

    with open(PATHS.content_index_html, 'r', encoding='utf-8') as html_src_file:
        html_content = html_src_file.read()
        with open(PATHS.public_index_html, 'w',
                  encoding='utf-8') as html_dst_file:
            html_dst_file.write(
                minify_html(html_content) if PRODUCTION_ENV_ else html_content)

    # --------------------------------------------------------------------------
    # COPY ENTIRE CONTENT/PUBLIC ASSETS DIR TO BUILD.

    copytree(src=PATHS.content_assets_dir, dst=PATHS.public_assets_dir,
             dirs_exist_ok=True)

    return 0
