# -*- coding: utf-8 -*-

from os import path
from subprocess import run
from sys import exit
from time import sleep

from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

from common import ProjectPaths

PATHS = ProjectPaths(path.dirname(path.dirname(__file__)))

is_script_running = False


class AppFSEventHandler(FileSystemEventHandler):
    def on_modified(self, event):
        global is_script_running

        sleep(0.200)

        if (not is_script_running) and (event.src_path.endswith('.html')
                                        or event.src_path.endswith('.css')):
            print(f'Changes detected in {event.src_path}. Restarting...')
            is_script_running = True
            # if sys.platform == "win32":
            run([PATHS.bat_script])
            is_script_running = False
            sleep(0.200)


def run_watcher() -> int:
    event_handler = AppFSEventHandler()
    observer = Observer()

    observer.schedule(event_handler=event_handler, path=PATHS.content_dir,
                      recursive=False)
    observer.start()

    try:
        print(f'Watching for changes...')
        observer.join()
    except KeyboardInterrupt:
        observer.stop()

    observer.join()
    return 0


if __name__ == '__main__':
    exit(run_watcher())
