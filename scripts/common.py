# -*- coding: utf-8 -*-

from os import path


class ProjectPaths:

    def __init__(self, root):
        """
        # Example usage

        project_paths = ProjectPaths(path.dirname(os.path.dirname(__file__)))

        # Accessing paths

        print(project_paths.root)
        print(project_paths.shell_script)
        print(project_paths.content_index_html)
        """

        self.root = root

        self.index_html_name = "index.html"
        self.style_css_name = "style.css"

        self.public_dir_name = "public"
        self.scripts_dir_name = "scripts"
        self.assets_dir_name = "assets"
        self.content_dir_name = "content"

        self.script_run_main_sh_linux_name = "run_main.sh"
        self.script_run_main_windows_name = "run_main.bat"

        self.public_dir = path.join(root, self.public_dir_name)
        self.scripts_dir = path.join(root, self.scripts_dir_name)
        self.content_dir = path.join(root, self.content_dir_name)

        self.content_index_html = path.join(self.content_dir,
                                            self.index_html_name)
        self.content_style_css = path.join(self.content_dir,
                                           self.style_css_name)
        self.content_assets_dir = path.join(self.content_dir,
                                            self.assets_dir_name)

        self.public_index_html = path.join(self.public_dir,
                                           self.index_html_name)
        self.public_style_css = path.join(self.public_dir, self.style_css_name)
        self.public_assets_dir = path.join(self.public_dir,
                                           self.assets_dir_name)

        self.shell_script = path.join(self.scripts_dir,
                                      self.script_run_main_sh_linux_name)
        self.bat_script = path.join(self.scripts_dir,
                                    self.script_run_main_windows_name)
